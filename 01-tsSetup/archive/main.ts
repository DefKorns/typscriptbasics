/*class SweetSweetBasil {
    name: string;
    color: string;

    constructor(){

    }
    getColor() {
        console.log(this.color);
    }
}
let world = new SweetSweetBasil();//"World", "blue");

class Basil extends SweetSweetBasil{
    constructor(name:string, startColor:string){
      super();
      this.name = name;
      this.color = startColor;
    }
    setNewColor(newColor:string){
        this.color = newColor;
    }

}

let basil3 = new Basil("basil", "bright green")
basil3.getColor();
basil3.setNewColor("Red");
basil3.getColor();
*/

import { MustHaveCoffee } from './src/coffee/getcoffee'

function f(input: boolean){
    let a = 100;
    if(input){
        let b = a + 10012;
        return b;
    }
    return a;
}

console.log(f(true));
console.log(f(false));

let coffee = new MustHaveCoffee();
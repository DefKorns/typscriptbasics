export class MustHaveCoffee {
    coffeeType: string;
    constructor() {
        console.log("Make it bulletproof");
    }

    setCoffeeType(name: string) {
        this.coffeeType = name;
    }
}